----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:55:28 04/28/2019 
-- Design Name: 
-- Module Name:    Multifix - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multifix is
    Port ( 
				-- INPUTS
				HSYNC     : in  STD_LOGIC;
				VSYNC     : in  STD_LOGIC;
				CSYNC     : in  STD_LOGIC;
				CLK14     : in  STD_LOGIC;
				CLK28     : in  STD_LOGIC;
				BLANK     : in  STD_LOGIC;
				SW_BYPASS : in  STD_LOGIC;  -- activate BYPASS
				SW_BLANK  : in  STD_LOGIC;  -- activate BLANK
--				SW_SOG    : in  STD_LOGIC;  -- activate Sync On Green

				-- OUTPUTS
				RE       : out  STD_LOGIC_VECTOR (2 downto 0);  -- Read Enable 0: for field memory
																				-- Read Enable 1: for direct line buffers
																				-- Read Enable 2: for field memory line buffers
				WE : out  STD_LOGIC;                        		-- Write Enable for field memory
				RST_R : out  STD_LOGIC_VECTOR (2 downto 0); 		-- Reset Read 0: Read Counter in field memory
																				-- Reset Read 1: Read Counter in direct line buffers
																				-- Reset Read 2: Read Counter in field memory line buffers
				RST_W : out  STD_LOGIC_VECTOR (2 downto 0); 		-- Reset Write 0: Write Counter in field memory
																				-- Reset Write 1: Write Counter in direct line buffers
																				-- Reset Write 2: Write Counter in field memory line buffers
				V_CLK : out  STD_LOGIC_VECTOR (2 downto 0);		-- Clock 0: Write Clock to all RAM
																				--          Read Clock for field memory
																				-- Clock 1: Read Clock for all line buffers
																				-- Clock 2: ADV7120/5 pixel clock

				BLANK_O : out  STD_LOGIC;		-- ADV7120/5 Blank input
				CSYNC_O : out  STD_LOGIC;		-- ADV7120/5 SYNC input for SOG 'Sync On Green'
				HSYNC_O : out  STD_LOGIC;		-- VGA HSYNC
				VSYNC_O : out  STD_LOGIC		-- VGA VSYNC
			);
 end Multifix;

architecture Behavioral of Multifix is

signal PIXEL_COUNTER: STD_LOGIC_VECTOR(9 downto 0) := (others =>'0');
signal LINE_COUNTER: STD_LOGIC_VECTOR(8 downto 0) := (others =>'0');
signal VGA_LINE_COUNTER: STD_LOGIC_VECTOR(9 downto 0) := (others =>'0');
signal HSYNC_D: STD_LOGIC := '0';
signal HSYNC_D2: STD_LOGIC := '0';

signal HSYNC_S: STD_LOGIC := '0';
signal HSYNC_SD: STD_LOGIC := '0';
signal VSYNC_S: STD_LOGIC := '0';
signal CSYNC_S: STD_LOGIC := '0';

signal HSYNC_1ST_LINE: STD_LOGIC := '0';
signal HSYNC_2ND_LINE: STD_LOGIC := '0';
signal FRAME_SYNC: STD_LOGIC := '0';
signal FRAME_TYPE: STD_LOGIC := '0';
signal TV_MODE: STD_LOGIC := '0';

signal RST_W_S: STD_LOGIC_VECTOR(2 downto 0) := "111";
signal RST_R_S: STD_LOGIC_VECTOR(2 downto 0) := "111";
signal RE_S: STD_LOGIC_VECTOR(2 downto 0) := "100";	
signal WE_S: STD_LOGIC := '1';	
signal BLANK_LINE_S: STD_LOGIC := '1';	
signal BLANK_S: STD_LOGIC := '1';	

--timing values 
--hex notation in vhdl : base#value#

--Amiga pal line is 908 clocks in 3,546895Mhz (C1) = 454 clocks in 7mhz -> 454 clocks in 14Mhz doubles the frequency
constant SCANLINE_WIDTH_PAL_C14 : integer := 908; 
constant SCANLINE_WIDTH_PAL_VGA : integer := SCANLINE_WIDTH_PAL_C14 / 2; 

--Amiga NTSC line is 910 clocks in 3,546895Mhz (C1) = 455 clocks in 7mhz -> 455 clocks in 14Mhz doubles the frequency
constant SCANLINE_WIDTH_NTSC_C14 : integer := 910;
constant SCANLINE_WIDTH_NTSC_VGA : integer := SCANLINE_WIDTH_NTSC_C14 / 2; 

--NTSC toggles between 227/228 NTSC-Clocks on a NTSC-system, BUT not on a PAL-System!

-- TV-Modes
constant NTSC : std_logic := '0';
constant PAL  : std_logic := '1';

-- Frame type
constant EVEN : std_logic := '0';
constant ODD  : std_logic := '1';

-- Frame mode
constant PROGRESSIVE : std_logic := '0';
constant INTERLACE   : std_logic := '1';

constant NTSC_LINES : integer := 263; --NTSC has 525 lines in progressive, 262/263 in interlace
constant NTSC_LINES_THREASHOLD : integer := NTSC_LINES; 
constant PAL_LINES : integer := 313; --PAL has 625 lines in progressive, 312/313 in interlace

constant NTSC_LINES_PROGRESSIVE : integer := 525; --NTSC has 525 lines in progressive, 262/263 in interlace
constant PAL_LINES_PROGRESSIVE : integer := 625; --PAL has 625 lines in progressive, 312/313 in interlace

-- H-SYNC
constant LINE_RESET_OFFSET : integer := -2;
constant HSYNC_1ST_TRIGGER_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA  + SCANLINE_WIDTH_PAL_VGA  + LINE_RESET_OFFSET; 
constant HSYNC_2ND_TRIGGER_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA  +                           LINE_RESET_OFFSET; 
constant HSYNC_1ST_TRIGGER_NTSC : integer := SCANLINE_WIDTH_NTSC_VGA + SCANLINE_WIDTH_NTSC_VGA + LINE_RESET_OFFSET; 
constant HSYNC_2ND_TRIGGER_NTSC : integer := SCANLINE_WIDTH_NTSC_VGA +							 LINE_RESET_OFFSET; 
constant HSYNC_PULSE_WIDTH : integer := 48; --can be anything between 30 and 65


--blanking
constant HSYNC_FRONTPORCH 	: integer := -22; --substract front porch
constant HSYNC_BACKPORCH 	: integer :=  22; --add back porch
constant BLANK_HSYNC_LINE0_FRONTPORCH_PAL 	: integer := HSYNC_1ST_TRIGGER_PAL + HSYNC_FRONTPORCH;
constant BLANK_HSYNC_LINE1_FRONTPORCH_PAL 	: integer := HSYNC_2ND_TRIGGER_PAL + HSYNC_FRONTPORCH;
constant BLANK_HSYNC_LINE0_BACKPORCH_PAL 	: integer := HSYNC_PULSE_WIDTH + HSYNC_BACKPORCH + LINE_RESET_OFFSET;
constant BLANK_HSYNC_LINE1_BACKPORCH_PAL 	: integer := HSYNC_PULSE_WIDTH + HSYNC_BACKPORCH + HSYNC_2ND_TRIGGER_PAL;
constant BLANK_HSYNC_LINE0_FRONTPORCH_NTSC 	: integer := HSYNC_1ST_TRIGGER_NTSC + HSYNC_FRONTPORCH;
constant BLANK_HSYNC_LINE1_FRONTPORCH_NTSC 	: integer := HSYNC_2ND_TRIGGER_NTSC + HSYNC_FRONTPORCH;
constant BLANK_HSYNC_LINE0_BACKPORCH_NTSC 	: integer := HSYNC_PULSE_WIDTH + HSYNC_BACKPORCH + LINE_RESET_OFFSET;
constant BLANK_HSYNC_LINE1_BACKPORCH_NTSC 	: integer := HSYNC_PULSE_WIDTH + HSYNC_BACKPORCH + HSYNC_2ND_TRIGGER_NTSC;


constant VSYNC_FRONTPORCH : integer := -6;
constant VSYNC_BACKPORCH : integer := 53;
constant BLANK_VSYNC_BACKPORCH_PROGRESSIVE 	: integer := VSYNC_BACKPORCH;
constant BLANK_VSYNC_BACKPORCH_INTERLACE 	: integer := VSYNC_BACKPORCH + 1;
constant BLANK_VSYNC_FRONTPORCH_NTSC : integer := NTSC_LINES_PROGRESSIVE + VSYNC_FRONTPORCH;
constant BLANK_VSYNC_FRONTPORCH_PAL  : integer := PAL_LINES_PROGRESSIVE	 + VSYNC_FRONTPORCH;


constant FIELD_MEM_WRITE_RESET_PAL 	: integer := 128; 
constant FIELD_MEM_WRITE_RESET_NTSC : integer := 128; 
constant FIELD_MEM_READ_ENABLE_START_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA;
constant FIELD_MEM_READ_ENABLE_START_NTSC 	: integer := SCANLINE_WIDTH_NTSC_VGA;
constant FIELD_MEM_READ_ENABLE_WIDTH_PAL 	: integer := FIELD_MEM_READ_ENABLE_START_PAL	+ FIELD_MEM_WRITE_RESET_PAL;
constant FIELD_MEM_READ_ENABLE_WIDTH_NTSC 	: integer := FIELD_MEM_READ_ENABLE_START_NTSC	+ FIELD_MEM_WRITE_RESET_NTSC;
constant FIELD_MEM_READ_RESET_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA  / 2; 
constant FIELD_MEM_READ_RESET_NTSC 	: integer := SCANLINE_WIDTH_NTSC_VGA / 2;

constant INTERLACE_LINE1_MEM_WRITE_RESET 		 : integer := 0;
constant INTERLACE_LINE1_MEM_1ST_READ_RESET 	 : integer := 0;
constant INTERLACE_LINE1_MEM_2ND_READ_RESET_PAL  : integer := SCANLINE_WIDTH_PAL_VGA;
constant INTERLACE_LINE1_MEM_2ND_READ_RESET_NTSC : integer := SCANLINE_WIDTH_NTSC_VGA;

constant INTERLACE_LINE2_MEM_READ_RESET_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA;
constant INTERLACE_LINE2_MEM_WRITE_RESET_PAL 	: integer := SCANLINE_WIDTH_PAL_VGA;
constant INTERLACE_LINE2_MEM_READ_RESET_NTSC 	: integer := SCANLINE_WIDTH_NTSC_VGA;
constant INTERLACE_LINE2_MEM_WRITE_RESET_NTSC 	: integer := SCANLINE_WIDTH_NTSC_VGA;

constant INTERLACE_DETECTION : integer := HSYNC_1ST_TRIGGER_NTSC / 2;

begin

	V_CLK(0) <= CLK14;
	V_CLK(1) <= CLK14 when SW_BYPASS ='0' else CLK28;
	V_CLK(2) <= CLK28;
	
	VSYNC_O  <= VSYNC when SW_BYPASS='0' else VSYNC_S;
	HSYNC_O  <= HSYNC when SW_BYPASS='0' else HSYNC_S;
	CSYNC_O  <= CSYNC when SW_BYPASS='0' else CSYNC_S;
	
	RE <="101" when SW_BYPASS='0' else RE_S;
	WE <='1'  when SW_BYPASS='0' else WE_S;
	RST_W <= ('0', HSYNC, '0')  when SW_BYPASS='0' else RST_W_S;
	RST_R <= ('0', HSYNC, '0')  when SW_BYPASS='0' else RST_R_S;

	BLANK_O <= '1' when SW_BLANK='0' else not BLANK when SW_BYPASS='0' else (BLANK_S and BLANK_LINE_S);

	signals: process(CLK14)
	begin
		if(falling_edge(CLK14))then
		
			-- HSYNC: signal sync			
			HSYNC_D2 <= HSYNC_D;
			HSYNC_D  <= HSYNC;
			HSYNC_SD <= HSYNC_S;

			-- VSYNC: signal sync for Edge detection
			VSYNC_S <= VSYNC;		
			
			--  HSYNC counter: catch HSYNC falling edge
			if(HSYNC = '0' and HSYNC_D = '1') then
				PIXEL_COUNTER <= (others =>'0');
			else
				PIXEL_COUNTER <= PIXEL_COUNTER+1;
			end if;
			
			--PAL/NTSC detection: NTSC has only 525 lines in progressive or 262.5 lines in interlace
			-- Reset line counters on frame start start
			if(VSYNC ='0' and VSYNC_S ='1') then
				if(LINE_COUNTER > NTSC_LINES_THREASHOLD	) then
					TV_MODE <= PAL;
				else
					TV_MODE <= NTSC;
				end if;
				 -- new Frame -> init counters
				 LINE_COUNTER <= (others =>'0');
				 VGA_LINE_COUNTER <= (others =>'0');
			else --here was a bug: elsif wasn't couznting correctly. THANK YOU, Marc!
				-- Amiga (=Input) line counter
				if(HSYNC_D = '0' and HSYNC_D2 = '1') then
					LINE_COUNTER <= LINE_COUNTER+1;
				end if;
				-- VGA (=Output) line counter
				if(HSYNC_S = '0' and HSYNC_SD = '1') then
					VGA_LINE_COUNTER <= VGA_LINE_COUNTER+1;
				end if;
			end if;				

			
			--FRAME_SYNC Signal
			-- catch VSYNC event to detect odd frame and interlace 
			-- this code was fixed by Marc to avoid a strange Amiga-chipsetbug:
			-- If the Amiga is switching from progressive to interlaced mode it might happen
			-- that the half line is on the EVEN frame and not on the ODD frame, where it belongs
			-- This made giana sisters' screen defect!
			-- THANK YOU!
			if( VSYNC = '1' and VSYNC_S = '0' ) then 
				if ( PIXEL_COUNTER >= INTERLACE_DETECTION )then
					if (FRAME_TYPE = EVEN)then
						FRAME_SYNC <= INTERLACE;
					else
						FRAME_SYNC <= PROGRESSIVE;
					end if;
					FRAME_TYPE <= ODD;
				else
				--FRAME_SYNC Signal (even frame)
					if (FRAME_TYPE = EVEN)then
						FRAME_SYNC <= PROGRESSIVE;
					end if;
					FRAME_TYPE <= EVEN;
				end if;
			end if;

		
			
			-- field mem control
			-- skip inactive pixel after hsync (beginning of each line)
			-- write enable on counter match
			if( 	(PIXEL_COUNTER(9 downto 0) < FIELD_MEM_WRITE_RESET_PAL and TV_MODE = PAL) 	or (PIXEL_COUNTER(9 downto 0) >= SCANLINE_WIDTH_PAL_C14  and TV_MODE = PAL ) or
					(PIXEL_COUNTER(9 downto 0) < FIELD_MEM_WRITE_RESET_NTSC and TV_MODE = NTSC) or (PIXEL_COUNTER(9 downto 0) >= SCANLINE_WIDTH_NTSC_C14 and TV_MODE = NTSC)
				)then
				WE_S <='0';  
			else
				WE_S <='1';  -- high active
			end if;
			
			-- read enable on counter match - width must be same as write!
			-- read has to start earliest at middle of expected line
			if(	(PIXEL_COUNTER(9 downto 0) >= FIELD_MEM_READ_ENABLE_START_PAL  and PIXEL_COUNTER(9 downto 0) < FIELD_MEM_READ_ENABLE_WIDTH_PAL  and TV_MODE = PAL) or
				(PIXEL_COUNTER(9 downto 0) >= FIELD_MEM_READ_ENABLE_START_NTSC and PIXEL_COUNTER(9 downto 0) < FIELD_MEM_READ_ENABLE_WIDTH_NTSC and TV_MODE = NTSC)
				)then
				RE_S(0) <='0';
			else
				RE_S(0) <='1';  -- read enable field memory
			end if;			
			
			--read reset: On Frame end reset
			if( VSYNC= '0' and(
					((	PIXEL_COUNTER(9 downto 0) = FIELD_MEM_READ_RESET_PAL or --first line
						PIXEL_COUNTER(9 downto 0) = (SCANLINE_WIDTH_PAL_VGA + FIELD_MEM_READ_RESET_PAL) ) and  TV_MODE = PAL) or--second line
					((PIXEL_COUNTER(9 downto 0) = FIELD_MEM_READ_RESET_NTSC --first line
					or PIXEL_COUNTER(9 downto 0) = (SCANLINE_WIDTH_NTSC_VGA + FIELD_MEM_READ_RESET_NTSC)) and  TV_MODE = NTSC) --second line
					)
				)then
				RST_R_S(0) <='1';  -- read reset field memory line buffer
			else
				RST_R_S(0) <='0';
			end if;
			
			-- write reset field memory: Just one clock after read reset
			RST_W_S(0)<=RST_R_S(0);
			
			
			-- VGA output
			
			-- scanlinebuffer read enable
			if( FRAME_SYNC = INTERLACE and ( 
					( PIXEL_COUNTER(9 downto 0) < SCANLINE_WIDTH_PAL_VGA  and TV_MODE = PAL) or
					( PIXEL_COUNTER(9 downto 0) < SCANLINE_WIDTH_NTSC_VGA and TV_MODE = NTSC) 	
					)									
					
				)then
				-- RE for SCANLINE buffer: low active
				RE_S(1) <='1';  
				RE_S(2) <='0';  -- read enable field memory line buffer
			else
				-- RE for SCANLINE buffer: low active
				RE_S(1) <='0';  -- read enable direct line buffer
				RE_S(2) <='1';  
			end if;			
		
			-- scanlinebuffer for first (half field) line or 2 lines in progressive mode
			-- read reset
			if( PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE1_MEM_1ST_READ_RESET or
				 (FRAME_SYNC = PROGRESSIVE and PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE1_MEM_2ND_READ_RESET_PAL  and TV_MODE = PAL) or
				 (FRAME_SYNC = PROGRESSIVE and PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE1_MEM_2ND_READ_RESET_NTSC and TV_MODE = NTSC)
				)then
				RST_R_S(1) <= '0';  -- reset read field memory line buffer
			else
				RST_R_S(1) <= '1';  
			end if;

			-- write reset to direct line buffer
			if( PIXEL_COUNTER(9 downto 0)=INTERLACE_LINE1_MEM_WRITE_RESET )then
				RST_W_S(1) <= '0';  -- reset write field memory line buffer
			else
				RST_W_S(1) <= '1';
			end if;

			-- scanlinebuffer for second (half field) line or disabled for progressive mode
			-- read reset
			if( ((PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE2_MEM_READ_RESET_PAL  and TV_MODE = PAL) or
				 (PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE2_MEM_READ_RESET_NTSC and TV_MODE = NTSC))
				 and FRAME_SYNC = INTERLACE
				)then
				RST_R_S(2) <= '0';
			else
				RST_R_S(2) <= '1';  -- reset read field memory line buffer
			end if;
			
			--write reset 
			if( (PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE2_MEM_WRITE_RESET_PAL  and TV_MODE = PAL) or
				(PIXEL_COUNTER(9 downto 0) = INTERLACE_LINE2_MEM_WRITE_RESET_NTSC and TV_MODE = NTSC)
				)then
				RST_W_S(2) <= '0';
			else
				RST_W_S(2) <= '1';
			end if;
			
			--HSYNC doubling

			if ( TV_MODE = PAL ) then

				--HSYNC_1ST_LINE 
				if( (PIXEL_COUNTER(9 downto 0)>= HSYNC_PULSE_WIDTH+LINE_RESET_OFFSET and PIXEL_COUNTER(9 downto 0) < HSYNC_1ST_TRIGGER_PAL )
					)then
					HSYNC_1ST_LINE <= '0';
				else
					HSYNC_1ST_LINE <= '1';
				end if;
			
				--HSYNC_2ND_LINE 
				if( (PIXEL_COUNTER(9 downto 0) >= HSYNC_2ND_TRIGGER_PAL  and PIXEL_COUNTER(9 downto 0)< HSYNC_2ND_TRIGGER_PAL  + HSYNC_PULSE_WIDTH) --trigger
					)then
					HSYNC_2ND_LINE <= '1';
				else
					HSYNC_2ND_LINE <= '0';
				end if;

			else -- NTSC

				--HSYNC_1ST_LINE 
				if((PIXEL_COUNTER(9 downto 0)>= HSYNC_PULSE_WIDTH + LINE_RESET_OFFSET and PIXEL_COUNTER(9 downto 0) < HSYNC_1ST_TRIGGER_NTSC)
					)then
					HSYNC_1ST_LINE <= '0';
				else
					HSYNC_1ST_LINE <= '1';
				end if;
				
				--HSYNC_2ND_LINE 
				if( (PIXEL_COUNTER(9 downto 0) >= HSYNC_2ND_TRIGGER_NTSC and PIXEL_COUNTER(9 downto 0)< HSYNC_2ND_TRIGGER_NTSC + HSYNC_PULSE_WIDTH) 
					)then
					HSYNC_2ND_LINE <= '1';
				else
					HSYNC_2ND_LINE <= '0';
				end if;

			end if;


			--HSYNC-Signal
			HSYNC_S <= not ( HSYNC_1ST_LINE or HSYNC_2ND_LINE );
			
			--blank vsync
			if( ( VGA_LINE_COUNTER(9 downto 0) <= BLANK_VSYNC_BACKPORCH_INTERLACE and FRAME_SYNC = INTERLACE)     or
				( VGA_LINE_COUNTER(9 downto 0) <= BLANK_VSYNC_BACKPORCH_PROGRESSIVE and FRAME_SYNC = PROGRESSIVE) or
				((VGA_LINE_COUNTER(9 downto 0) >= BLANK_VSYNC_FRONTPORCH_PAL  ) and TV_MODE = PAL ) or
				((VGA_LINE_COUNTER(9 downto 0) >= BLANK_VSYNC_FRONTPORCH_NTSC ) and TV_MODE = NTSC )
				 )then
				BLANK_LINE_S <='0';
			else
				BLANK_LINE_S <='1';
			end if;
			
			--blank hsync
			if( ((PIXEL_COUNTER(9 downto 0) >= BLANK_HSYNC_LINE1_FRONTPORCH_PAL  and PIXEL_COUNTER(9 downto 0)< BLANK_HSYNC_LINE1_BACKPORCH_PAL ) and TV_MODE = PAL ) or
				((PIXEL_COUNTER(9 downto 0) >= BLANK_HSYNC_LINE1_FRONTPORCH_NTSC and PIXEL_COUNTER(9 downto 0)< BLANK_HSYNC_LINE1_BACKPORCH_NTSC) and TV_MODE = NTSC) or
				((PIXEL_COUNTER(9 downto 0) >= BLANK_HSYNC_LINE0_FRONTPORCH_PAL  or  PIXEL_COUNTER(9 downto 0)< BLANK_HSYNC_LINE0_BACKPORCH_PAL ) and TV_MODE = PAL ) or
				((PIXEL_COUNTER(9 downto 0) >= BLANK_HSYNC_LINE0_FRONTPORCH_NTSC or  PIXEL_COUNTER(9 downto 0)< BLANK_HSYNC_LINE0_BACKPORCH_NTSC) and TV_MODE = NTSC) 
				 )then
				BLANK_S <= '0';
			else
				BLANK_S <= '1';
			end if;
			
			--construct csync
			if( VSYNC ='1' )then
				CSYNC_S <=HSYNC_S;
			else
				CSYNC_S <= not HSYNC_S;
			end if;
		end if;
	end process;
	

end Behavioral;

