# Multifix-AGA
 
This is a ECS and AGA-combo scandoubler and flicker fixer for big box Amigas.
![Prototype rev 0.1](https://gitlab.com/MHeinrichs/Multifix-AGA/raw/master/Pics/klein/DSC_2306-k.jpg)

A scandoubler doubles the horizontal frequency of the Amiga output from ~15.6kHz to ~31.5kHz to let a VGA monitor think it has the double number of lines. But in real every scanline is just doubbled ;).
In interlace two suceeding lines are transmitted via half frames @25Hz. A flicker fixer stores one frame in a field mem and uses it to complete the second frame @ 50 Hz. It removes the nasty interlace flicker.

Two switches allow some nice options:
Switch 1 activates a true bypass, for Multisync-Monitors
Switch 2 activates a true blank-signal for full black detection: many TFT-Displays display a dark grey instead of a real black. This fixes this issue for some games. However the Workbench will get much too dark with this option activated.

Other nice features:

	* Sync on green capability
	* DiD (digital interface Data for Monitors- Adpater: If you own a i�C-controller for the Amiga you can read the supported resolutions from your monitor.
	* RTG-Loop-through for my RTG-graphic card project

Currently following resolutions are supported:
	* LoRes
	* LoRes Interlace
	* HiRes
	* HiRes Intelace

All above are supported in PAL and NTSC Mode. However, the PAL Amiga cheats a bit on NTSC timing, thus many TFT show some strange vertical line artefarcts in NTSC-mode.

**Revision 3 is tested!**

**BUT ONLY on PAL-AMIGAS! NTSC-Amigas have not been tested and require a different timing!**

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of six 256kByte field memories and six 1k line buffers. A CPLD and a PLL to generate a 28MHz clock synchronious to the video clock of the Amiga (C1)
* The CPLD does various things:
    * It detects PAL and NTSC
    * It detects progressive and interlace modes
    * It times all the memory accesses 

## What is in this repository?
This repository consists of several directories:
* Layout: The board, schematic and bill of material (BOM) are in this directory.
* Logic: Here the code, project files and pin-assignments for the CPLD is stored
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. The field and line rams and the ADV7120 DAC are the most difficult part to get. 
A good start for parts is [this list](https://www.reichelt.de/my/1644061) for Rev. 3.


## How to programm the board?
The CPLD must be programmed via Xilinx IMpact and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?p=1286234). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

